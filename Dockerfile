FROM ubuntu:22.04

# update packages 
RUN  apt update -qy \
&&  apt -qy full-upgrade

# Set the locale
RUN DEBIAN_FRONTEND=noninteractive TZ=Etc/UTC apt -qy install tzdata
ENV LANG C.UTF-8
ENV LC_ALL C.UTF-8
ENV DEBIAN_FRONTEND noninteractive
RUN apt install -qy locales
RUN locale-gen

# install software and cleanup packages 
COPY utils/scripts/apt_packages_to_install.txt .
RUN xargs apt install -qy < apt_packages_to_install.txt
RUN apt autoremove -qy && apt clean -qy \
&& rm -rf /var/cache/apt/archives/

# Create a user with passwordless sudo
ARG USERNAME=kfp
ARG USER_UID=1000
ARG USER_GID=$USER_UID
RUN groupadd --gid $USER_GID $USERNAME \
    && useradd --uid $USER_UID --gid $USER_GID -m $USERNAME \
    && echo $USERNAME ALL=\(root\) NOPASSWD:ALL > /etc/sudoers.d/$USERNAME \
    && echo '%sudo ALL=(ALL) NOPASSWD:ALL' >> /etc/sudoers \
    && chmod 0440 /etc/sudoers.d/$USERNAME

# copy files into the container 
WORKDIR /home/$USERNAME
COPY --chown=$USERNAME:$USERNAME . .
USER $USERNAME

# install latest version of go and rust 
RUN ["/bin/zsh", "-c", "./utils/scripts/install_go.sh"]
RUN ["/bin/zsh", "-c", "./utils/scripts/install_rust.sh -y"]
# install and setup : fzf, pip, OhMyZsh, asdf, starship, spacevim, tmux, arkade, krew, telepresense, homebrew
RUN ["/bin/zsh", "-c", "./utils/scripts/install_omzsh.sh"]
RUN ["/bin/zsh", "-c", "./utils/scripts/setup_omz_plugins.sh"]
COPY --chown=$USERNAME:$USERNAME utils/dotfiles/zsh .
RUN ["/bin/zsh", "-c", "./utils/scripts/install_fzf.sh"]
RUN ["/bin/zsh", "-c", "source ./utils/scripts/setup_pip.sh"]
RUN ["/bin/zsh", "-c", "./utils/scripts/install_asdf.sh"]
RUN ["/bin/zsh", "-c", "./utils/scripts/setup_asdf.sh"]
COPY --chown=$USERNAME:$USERNAME utils/dotfiles/asdf/.tool-versions .
RUN ["/bin/zsh", "-c", "./utils/scripts/install_starship.sh --yes"]
COPY --chown=$USERNAME:$USERNAME utils/dotfiles/starship .
RUN ["/bin/zsh", "-c", "./utils/scripts/install_spacevim.sh"]
COPY --chown=$USERNAME:$USERNAME utils/dotfiles/vim/init.toml /home/user/.SpaceVim.d/init.toml
RUN ["/bin/zsh", "-c", "./utils/scripts/install_arkade.sh"]
RUN ["/bin/zsh", "-c", "./utils/scripts/setup_arkade.sh"]
RUN ["/bin/zsh", "-c", "./utils/scripts/install_tmux_samoskin.sh"]
RUN ["/bin/zsh", "-c", "./utils/scripts/install_krew.sh"]
RUN ["/bin/zsh", "-c", "./utils/scripts/setup_krew.sh"]
RUN ["/bin/zsh", "-c", "sudo curl -fL https://app.getambassador.io/download/tel2/linux/amd64/latest/telepresence -o /usr/local/bin/telepresence"]
RUN ["/bin/zsh", "-c", "./utils/scripts/install_homebrew.sh"]
RUN ["/bin/zsh", "-c", "./utils/scripts/setup_homebrew.sh"]

ENTRYPOINT ["/bin/zsh"]
ENV USER=$USERNAME